﻿/*
 *  FILENAME        : IISSInstaller.cs
 *  PROJECT         : IISS
 *  PROGRAMMER      : Jody Markic, Nathan Bray & Gabriel Paquette
 *  FIRST VERSION   : 12/10/2016
 *  DESCRIPTION     : This file holds the ConfigureServer class that reads a config file, and
 *                    sets up the server based off the parameters provided in the config file.
 * 
 */
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Reflection;

namespace Configurations
{
    //
    //  CLASS       : ConfigureServer
    //  DESCRIPTION : This class holds methods that parse and interpret the IISS.config file's contents,
    //                to setup up the server with the approriate path, port, and default page.
    //
    public class ConfigureServer
    {
        public static string _ROOTDIR { get; set; }
        public static string _DEFAULT { get; set; }
        public static string _PORT { get; set; }
        public static string _SERVER_IP { get; set; }

        private static string installDir = Path.GetDirectoryName(Assembly.GetEntryAss‌​embly().Location);
        public static string configFile = installDir + "\\IISS.ini";

        //
        //  METHOD      : configureServer()
        //  DESCRIPTION : this method checks to see if the config file exists, if not it creates one,
        //                then conintues to read the contents the file, and calls parseConfigFile, along
        //                with getting the host machines IP address.
        //  PARAMETER   : N/A
        //  RETURNS     : N/A
        //
        public static void configureServer()
        {
            if (!File.Exists(configFile))
            {
                using (StreamWriter file = new StreamWriter(@configFile))
                {
                    file.Write("ROOTDIR=" + installDir + "\\wwwroot\r\nDEFAULT=index.html\r\nPORT=13000\r\n");
                }
            }
            string fileContents = File.ReadAllText(configFile);
            parseConfigFile(fileContents);
            GetLocalIPAddress();
        }


        //
        //  METHOD      : parseConfigFile()
        //  DESCRIPTION : This method parse the file contents and stores them into static variables
        //                to be used by the server
        //  PARAMETER   : string : fileContents
        //  RETURNS     : N/A
        //
        public static void parseConfigFile(string fileContents)
        {
            string[] fileLines;
            List<string> configureInformation = new List<string>();
            // remove the \r
            fileContents = fileContents.Replace("\r", "");
            // Seperate each line in the file
            char[] seperator = { '\n' };
            //split each line on the '='
            fileLines = fileContents.Split(seperator, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < fileLines.Length; i++)
            {
                string[] parsedLine = fileLines[i].Split('=');
                //gather each value for each line
                configureInformation.Add(parsedLine[1]);
                Console.WriteLine(configureInformation[i]);
            }
            _ROOTDIR = configureInformation[0] + "\\";
            _DEFAULT = configureInformation[1];
            _PORT = configureInformation[2];
        }


        //
        //  METHOD      : GetLocalIPAddress()
        //  DESCRIPTION : Get the Local IP address of the host machine
        //  PARAMETER   : N/A
        //  RETURNS     : N/A
        //
        public static void GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    _SERVER_IP = ip.ToString();
                    break;
                }
            }
        }
    }
}
