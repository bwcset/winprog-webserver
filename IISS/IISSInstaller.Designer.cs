﻿namespace IISS
{
    partial class IISSInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IISSProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.IISSServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // IISSProcessInstaller
            // 
            this.IISSProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.IISSProcessInstaller.Password = null;
            this.IISSProcessInstaller.Username = null;
            // 
            // IISSServiceInstaller
            // 
            this.IISSServiceInstaller.Description = "Internet Information SET Services (IISS) is a web service for SSP development. ";
            this.IISSServiceInstaller.DisplayName = " IISS Web Server";
            this.IISSServiceInstaller.ServiceName = "IISS";
            this.IISSServiceInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.IISSServiceInstaller_AfterInstall);
            this.IISSServiceInstaller.BeforeUninstall += new System.Configuration.Install.InstallEventHandler(this.IISSServiceInstaller_BeforeUninstall);
            // 
            // IISSInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.IISSProcessInstaller,
            this.IISSServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller IISSProcessInstaller;
        private System.ServiceProcess.ServiceInstaller IISSServiceInstaller;
    }
}