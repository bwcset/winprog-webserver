﻿/*
 *  FILENAME        : IISSInstaller.cs
 *  PROJECT         : IISS
 *  PROGRAMMER      : Jody Markic, Nathan Bray & Gabriel Paquette
 *  FIRST VERSION   : 12/10/2016
 *  DESCRIPTION     : This file holds the SETService class, it has three methods associated with it
 *                    a constructor and two event handles on start and stop of the service.
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IISS
{
    //
    //  CLASS       : SETService
    //  DESCRIPTION : This class starts the HTTP Server service, it has event handles on start and stop,
    //                That log to the Service Event Log.
    //
    public partial class SETService : ServiceBase
    {
        Thread program;
        Server webService;

        //
        //  METHOD      : SetService()
        //  DESCRIPTION : Constructor that threads the Server and initializes the service
        //  PARAMETER   : N/A
        //  RETURNS     : N/A
        //
        public SETService()
        {
            InitializeComponent();
            webService = new Server();
            program = new Thread(webService.StartServer);
        }

        //
        //  METHOD      : OnStart
        //  DESCRIPTION : This Method is an event to the service on start, it concurrently start the server,
        //                and logs the event.
        //  PARAMETER   : string[]  : args
        //  RETURNS     : N/A
        //
        protected override void OnStart(string[] args)
        {
            Logger.Log("Initializing IISS Web Server.");
            program.Start();
        }

        //
        //  METHOD      : OnStop()
        //  DESCRIPTION : This Method is an even tot the service on stop, it conncurrently stops the server,
        //                and logs the event
        //  PARAMETER   : N/A
        //  RETURNS     : N/A
        //
        protected override void OnStop()
        {
            webService.shutdownServer();
            Logger.Log("Stopped IISS Web Server.");
        }
    }
}
