﻿/*
 *  FILENAME        : IISSInstaller.cs
 *  PROJECT         : IISS
 *  PROGRAMMER      : Jody Markic, Nathan Bray & Gabriel Paquette
 *  FIRST VERSION   : 12/10/2016
 *  DESCRIPTION     : This file holds the the Interpreter class that reads a ssc script file
 *                    and parses then interprets it so the server can determine what to do with
 *                    it.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace IISS
{
    //
    //  CLASS       : Inerpreter
    //  DESCRIPTION : This class handles ssc files for the server. It tokenizes the contents of it to get values,
    //                and assigns a type to these values to construct an approriate response. The Intepreter allows the server
    //                to handle simple concatination, retrieving current date and time, along with addition, multiplication,
    //                and subtraction of numerical values.
    //
    public class Interpreter
    {
        private static string[] keyWord = { "pr" };

        //
        //  METHOD      : startProcess
        //  DESCRIPTION : This Method takes a ssc script, and begins to process it through, finding
        //                beginning and ending script tags and processing whats inbetween
        //  PARAMETER   : string : fileName
        //                out string : fileResult
        //  RETURNS     : bool : retCode
        //
        public static bool startProccess(string fileName, out string fileResult)
        {
            bool retCode = true;
            List<int> startOfScript = new List<int>();
            List<int> endOfScript = new List<int>();
            string[] text = null;
            try
            {
                // read a lines from ssc file
                text = File.ReadAllLines(@fileName);
                for (int i = 0; i < text.Length; i++)
                {
                    //find start of script
                    if (text[i].IndexOf("<%") != -1)
                    {
                        startOfScript.Add(i + 1);
                    }
                    //find the end
                    if (text[i].IndexOf("%>") != -1)
                    {
                        endOfScript.Add(i - 1);
                    }

                }
                string retString = "";
                //check if we both have a start and end tag script tag
                if (startOfScript.Count == endOfScript.Count)
                {
                    for (int i = 0; i < startOfScript.Count; i++)
                    {
                        //process all lines within script tags
                        for (int j = startOfScript[i]; j <= endOfScript[i]; j++)
                        {
                            try
                            {
                                retString += processLine(text[j]);
                            }
                            catch (Exception)
                            {
                                retString += "Syntax Error at line " + (j + 1);
                                retCode = false;
                            }
                        }
                        //do something with retcode
                        for (int j = (startOfScript[i] - 1); j <= (endOfScript[i] + 1); j++)
                        {
                            text[j] = "";
                        }
                        text[startOfScript[i] - 1] = retString;
                        //clear retstring
                        retString = "";
                    }
                }
                else
                {
                    //if script tag's aren't provided.
                    string tag = "";
                    if (startOfScript.Count < endOfScript.Count)
                    {
                        tag = "<%";
                    }
                    else
                    {
                        tag = "%>";
                    }
                    Array.Clear(text, 0, text.Length);
                    text[0] = "Syntax Error Occurred. Found: '' Expected: '" + tag + "'";
                    retCode = false;
                }
            }
            catch (Exception e)
            {
                //if any other exception occurs
                Array.Clear(text, 0, text.Length);
                text[0] = "Unknown Error Occurred: " + e.Message;
                retCode = false;
            }
            //fileResult = text;

            fileResult = string.Join("",text);
            return retCode;
        }

        //
        //  METHOD      : processLine
        //  DESCRIPTION : This method cuts a ssc statement into just the body of the statement,
        //                to be later tokenized by SplitInside
        //  PARAMETER   : string : line
        //  RETURNS     : string : retString
        //
        private static string processLine(string line)
        {
            //trim leading or trailing white space.
            string retString = "";
            line = line.Trim();

            //find the end of a script statement.
            int semi = line.IndexOf(';');
            string inside = "";

            //string[] tokens = { };

            List<string> tokens = new List<string>();
            List<string> typeOfTokens = new List<string>();

            // if pr is provided
            if (line.IndexOf("pr") == 0)
            {
                //get whats inside of the statement
                inside = line.Substring(3, semi - 3);

                // split value and evaluate type of value
                tokens = splitInside(inside, out typeOfTokens);

                //concatinate now deconstructed values to a string
                retString += contructString(tokens, typeOfTokens);
            }

            return retString;

        }

        //
        //  METHOD      : splitInside()
        //  DESCRIPTION : This Method tokenizes a particular ssc statement to key values pairs of a value and type,
        //                and stores each piece of the pair into List<string> respectively.
        //  PARAMETER   : string inside
        //                out List<string> tokenType
        //  RETURNS     : List<string> : token
        //
        private static List<string> splitInside(string inside, out List<string> tokenType)
        {
            List<string> token = new List<string>();
            List<string> tokenT = new List<string>();
            bool stringStartBool = false;
            bool stringEndBool = false;

            int start = -1;
            int end = -1;


            for (int i = 0; i < inside.Length; i++)
            {
                char currentChar = inside[i];

                //this will parse out a full string
                if (currentChar == '\"')
                {
                    if (stringStartBool == false)
                    {
                        stringStartBool = true;
                        start = i + 1;
                    }
                    else
                    {
                        stringEndBool = true;
                        end = i;
                    }
                }
                // if a string is not currently being read
                else if (stringStartBool == false && stringEndBool == false)
                {
                    // search for the values & for concatination
                    if (currentChar == '&')
                    {
                        token.Add(currentChar.ToString());
                        tokenT.Add("CONCAT");
                    }
                    // date() for current date
                    else if (inside.IndexOf("date()") == i)
                    {
                        token.Add(DateTime.Now.ToString("yyyy-MM-dd"));
                        tokenT.Add("DATE");
                        i += 5;
                    }
                    //time() for current time
                    else if (inside.IndexOf("time()") == i)
                    {
                        token.Add(DateTime.Now.ToString("hh:mm:ss"));
                        tokenT.Add("TIME");
                        i += 5;
                    }
                    // else are the numerical
                    else
                    {
                        //for a numerical value
                        if (char.IsDigit(currentChar))
                        {
                            token.Add(currentChar.ToString());
                            tokenT.Add("DIGIT");
                        }
                        // * for multiplication
                        else if (currentChar == '*')
                        {
                            token.Add(currentChar.ToString());
                            tokenT.Add("MULTI");
                        }
                        // + for addition
                        else if (currentChar == '+')
                        {
                            token.Add(currentChar.ToString());
                            tokenT.Add("PLUS");
                        }
                        // - for subtraction
                        else if (currentChar == '-')
                        {
                            token.Add(currentChar.ToString());
                            tokenT.Add("MINUS");
                        }
                    }
                }


                // if our value has both a start " and end "
                if (stringStartBool && stringEndBool)
                {
                    // the inside is a string
                    token.Add(inside.Substring(start, end - start));
                    stringStartBool = false;
                    stringEndBool = false;
                    start = -1;
                    end = -1;
                    tokenT.Add("STRING");
                }
            }

            tokenType = tokenT;
            return token;
        }

        //
        //  METHOD      : constructString
        //  DESCRIPTION : This takes the interpreted string and does the request on it, which includes,
        //                concatination, providing date or current time, and addition, subtraction, or multiplication
        //  PARAMETER   : List<string> tokens,
        //                List<string> typeOfTokens
        //  RETURNS     : string retString
        //
        private static string contructString(List<string> tokens, List<string> typeOfTokens)
        {
            string retString = "";
            string leftDigit = "";
            string rightDigit = "";
            bool switchSides = false;

            char operationType = ' ';

            // for each token
            for (int i = 0; i < tokens.Count; i++)
            {
                // check the type of the current token
                switch (typeOfTokens[i])
                {
                    // if string, date , or time
                    case "STRING":
                    case "DATE":
                    case "TIME":
                        if (i > 0)
                        {
                            // if request is to concatinate
                            if (typeOfTokens[i - 1] == "CONCAT")
                            {
                                // if value is not whitespce
                                if (tokens[i] != "")
                                {
                                    //concatinate tokens to a string
                                    retString += tokens[i];
                                }

                            }
                        }
                        else
                        {
                            //if this the first token assign it
                            retString = tokens[i];
                        }
                        break;
                    //case numerics
                    case "DIGIT":
                        // if we've run across an even sequence of digits
                        if (switchSides == true)
                        {
                            rightDigit += tokens[i];
                        }
                        // else an odd sequence(first time)
                        else
                        {
                            leftDigit += tokens[i];
                        }
                        break;
                     //case ran across multiply
                    case "MULTI":
                        operationType = '*';
                        switchSides = true;
                        break;
                    //case ran across addition
                    case "PLUS":
                        operationType = '+';
                        switchSides = true;
                        break;
                    //case ran across substraction
                    case "MINUS":
                        operationType = '-';
                        switchSides = true;
                        break;
                    // do math 
                    case "CONCAT":
                        if (leftDigit != "" && rightDigit != "")
                        {
                            //determine which mathimatical operation
                            switch (operationType)
                            {
                                case '*': retString += ((Convert.ToDouble(leftDigit) * Convert.ToDouble(rightDigit)).ToString()); break;
                                case '+': retString += ((Convert.ToDouble(leftDigit) + Convert.ToDouble(rightDigit)).ToString()); break;
                                case '-': retString += ((Convert.ToDouble(leftDigit) - Convert.ToDouble(rightDigit)).ToString()); break;
                            }
                            // clear variables for reuse
                            leftDigit = "";
                            rightDigit = "";
                            switchSides = false;
                            operationType = ' ';
                        }
                        break;
                }
            }
            // do math
            if (leftDigit != "" && rightDigit != "")
            {
                //determine which mathimatical operation
                switch (operationType)
                {
                    case '*': retString += ((Convert.ToDouble(leftDigit) * Convert.ToDouble(rightDigit)).ToString()); break;
                    case '+': retString += ((Convert.ToDouble(leftDigit) + Convert.ToDouble(rightDigit)).ToString()); break;
                    case '-': retString += ((Convert.ToDouble(leftDigit) - Convert.ToDouble(rightDigit)).ToString()); break;
                }
                // clear variables for reuse
                leftDigit = "";
                rightDigit = "";
                switchSides = false;
                operationType = ' ';
            }

            return retString;
        }
    }
}
