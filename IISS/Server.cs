﻿/*
 *  FILENAME        : Server.cs
 *  PROJECT         : WebDev_Assignment05_Server
 *  PROGRAMMER      : Jody Markic
 *  FIRST VERSION   : 11/25/2016
 *  DESCRIPTION     : This project accepts requests from a browser via TCP listener. The browser sends
 *                    a request via the HTTP protocal 1.1 along with a file, the Server searches for the file
 *                    and outputs it back to browser, if the file isn't found, the Server sends back an Error 404.
 */
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using Configurations;
using System.Collections.Generic;
using System.Linq;

namespace IISS
{
    //
    //  CLASS       : Server
    //  DESCRIPTION : This project accepts requests from a browser via TCP listener. The browser sends
    //                a request via the HTTP protocal 1.1 along with a file, the Server searches for the file
    //                and outputs it back to browser, if the file isn't found, the Server sends back an Error 404.
    //
    public class Server
    {
        private static TcpListener server;
        private static bool needClosing;


        //
        //  METHOD      : StartServer
        //  DESCRIPTION : This function starts the service
        //  RETURNS     : void
        //
        public void StartServer()
        {           
            server = null;
            ConfigureServer.configureServer();
            Logger.Log("Checking configurations... - Good");           
            StartListener();
            needClosing = false;

        }

        //
        //  METHOD      : StartListener
        //  DESCRIPTION : This function creates a local end point and sets a TCPListener to and calls
        //                listenForRequests that listens for incoming requests.
        //  PARAMETERS  : string : path
        //                string : argIP
        //                string : argPort
        //  RETURNS     : void
        //
        private void StartListener()
        {
            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = Int32.Parse(ConfigureServer._PORT); // Int32.Parse(argPort)
                IPAddress localAddr = IPAddress.Parse(ConfigureServer._SERVER_IP); //argIP
                // TcpListener server = new TcpListener(port);
                if (server != null)
                {
                    server.Stop(); 
                }
                server = new TcpListener(localAddr, port);
                // Start listening for client requests.
                server.Start();
                ListenForRequests();
                Thread.Sleep(1000);
            }
            catch (SocketException e)
            {
                Logger.Log("SocketException: "+ e);
                
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();
            }
        }

        //
        //  METHOD      : ListenForRequests
        //  DESCRIPTION : This function sits and waits for incoming requests
        //                once a request is recieved, it calls ParseRequest()
        //                to begin processing
        //  PARAMETERS  : TcpListener : server
        //                string      : path
        //  RETURNS     : void
        //
        private void ListenForRequests()
        {
            while (!needClosing)
            {
                try
                {
                    // Perform a blocking call to accept requests.
                    TcpClient client = server.AcceptTcpClient();
                    // Get a stream object for reading and writing
                    Thread thread = new Thread(() => ParseRequest(client));
                    Logger.Log("Client Connected");
                    thread.Start();
                }
                catch (Exception)
                {                    
                    break;
                }
            }
        }

        //
        //  METHOD      : ParseRequest
        //  DESCRIPTION : This function converts the contents of the request from a byte
        //                to a string and parses out the file requested to search for
        //                it calls ProcessRequest for further processing
        //  PARAMETERS  : TcpClient     : client
        //                NetworkStream : stream
        //                string        : path
        //  RETURN      : void
        //
        private void ParseRequest(TcpClient client)
        {
            Byte[] bytes = new Byte[5000];
            string data = null;
            // Get a stream object for reading and writing
            NetworkStream stream = client.GetStream();
            int i;
            // Loop to receive all the data sent by the client.
            if((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                // Translate data bytes to a ASCII string.
                data = Encoding.ASCII.GetString(bytes, 0, i);
                string request = getFileName(data);
                ProcessRequest(client,stream, request);
                Thread.Sleep(100);
            }
        }

        //
        //  METHOD      : processRequest
        //  DESCRIPTION : This function parses the file extention from file name,
        //                depending on the extention it calls the functions either SendTextResponse,
        //                SendImageResponse, or ErrorNotFound that respond back to the browser.
        //  PARAMETERS  : NetworkStream : stream
        //                string        : request
        //                string        : path
        //  RETURN      : void
        //
        private void ProcessRequest(TcpClient client, NetworkStream stream, string request)
        {
            //parse the file extention from the file name
            int startIndex = request.IndexOf(".");
            string extension = request.Substring(startIndex + 1);
            int statusCode = 0;
            string type = "";

            byte[] body = createMessage(ConfigureServer._ROOTDIR + request, extension, out type, out statusCode);
            // as long as the message was formed properly, we can send it
            if (body != null)
            {
                byte[] httpHeader = createHeader(statusCode, type, body.Length);

                // Send back a response.
                stream.Write(httpHeader, 0, httpHeader.Length);
                stream.Write(body, 0, body.Length);
                stream.Flush();
                
            }
            stream.Close();
            // close the connection
            client.Close();
        }

        //
        //  METHOD          : createMessage
        //  DESCRIPTIONS    : This method will create what is to be displayed to the user as a response after
        //                    parsing through what they requested
        //  PARAMETERS      : string    : fileName
        //                    string    : extension
        //                    out string: type
        //                    out int   : status
        //  RETURNS         : byte[]    : file
        //
        private byte[] createMessage(string fileName, string extension, out string type, out int status)
        {
            byte[] file = null;
            string sspContents = null;
            bool result = false;
            try
            {
                if (extension == "ssp")
                {
                    result = Interpreter.startProccess(fileName, out sspContents);
                    file = Encoding.ASCII.GetBytes(sspContents);
                }
                else
                {
                    file = File.ReadAllBytes(fileName);
                }
                type = getMimeType(extension);
                status = 200;
            }
            // If the file or directory to the file they specify does not exist, it will form a 404 message
            catch (FileNotFoundException)
            {
                if (extension != "ico")
                {
                    file = Encoding.ASCII.GetBytes("<html><head><title>Error 404.0 - Not Found</title></head>" +
                                "<body><h1>HTTP Error 404.0 - Not Found</h1></html>");
                }
                status = 404;
                type = "text/html";
            }
            catch (UnauthorizedAccessException)
            {
                file = Encoding.ASCII.GetBytes("<html><head><title>Error 403.0</title></head>" +
                            "<body><h1>HTTP Error 403.0 - Forbidden</h1></html>");
                status = 403;
                type = "text/html";
            }
            catch (Exception)
            {
                file = Encoding.ASCII.GetBytes("<html><head><title>Server Error</title></head>" +
                            "<body><h1>HTTP Error 500 - Internal Server Error</h1></html>");
                status = 500;
                type = "text/html";
            }

            return file;
        }

        //
        //  METHOD          : createHeader
        //  DESCRIPTIONS    : This method will creather the http Header for the response to the user
        //  PARAMETERS      : int       : status
        //                    string    : type
        //                    int       : bodyLength
        //  RETURNS         : byte[]    : returnArray
        //
        private byte[] createHeader(int status, string type, int bodyLength)
        {
            string header = "";
            string state = getState(status);
            byte[] returnArray = null;            
            // Form the header using the state, date, content type, and length of the body
            header = "HTTP/1.1" + state + "\r\n"
                    + "Date: " + DateTime.UtcNow + "\r\n"
                    + "Content-Type: " + type + "\r\n"
                    + "Content-Length: " + bodyLength + "\r\n"
                    + "\r\n";
            returnArray = Encoding.ASCII.GetBytes(header);


            return returnArray;
        }


        //
        //  METHOD          : getMimeType
        //  DESCRIPTIONS    : This method will take extension of the filename, and use it to determine the type
        //                    of content that is to be sent back
        //  PARAMETERS      : string    : extention
        //  RETURNS         : string    : mimeType
        //
        private string getMimeType(string extension)
        {
            string mimeType = "";
            switch (extension)
            {
                case "png":
                    mimeType = "image/png";
                    break;
                case "jpg":
                case "jpeg":
                    mimeType = "image/jpeg";
                    break;
                case "gif":
                    mimeType = "image/gif";
                    break;
                case "htm":
                case "html":
                case "htmls":
                case "ssp":
                    mimeType = "text/html";
                    break;
                case "ico":
                    mimeType = "image/x-icon";
                    break;
                default:
                    mimeType = "text/plain";
                    break;
            }
            return mimeType;
        }

        //
        //  METHOD          : getState()
        //  DESCRIPTIONS    : This method determines the state of the http response
        //                    being sent back to the browser
        //  PARAMETER       : int       : statusCode
        //  RETURNS         : string    : ret
        //
        private string getState(int statusCode)
        {
            string ret = "";
            switch (statusCode)
            {
                case 200:
                    ret = "200 OK";
                    break;
                case 404:
                    ret = "404 Not Found";
                    break;
                case 403:
                    ret = "403 Forbidden";
                    break;
                case 500:
                    ret = "500 Internal Server Error";
                    break;
                default:
                    break;
            }
            return ret;
        }

        //
        //
        //  METHOD          : getFileName
        //  DESCRIPTIONS    : This method will parse through the request for the name of the file in the GET header
        //                    and will return that.
        //  PARAMETERS      : string    : data
        //  RETURNS         : string    : file
        //
        private string getFileName(string data)
        {
            string file = "";
            int index = data.IndexOf('/');
            int last = data.IndexOf("HTTP/1.1");
            file = data.Substring(index, last - index - 1);
            if (file == "/")
            {
                file = "/index.html";
            }
            return file;
        }

        //  
        //  METHOD      : shutdownServer()
        //  DESCRIPTION : Changes boolean so server succesfully closes
        //  PARAMETERS  : N/A
        //  RETURN      : N/A
        //
        public void shutdownServer()
        {
            needClosing = true;
            server.Stop();
        }
    }
}