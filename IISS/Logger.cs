﻿/*
 *  FILENAME        : IISSInstaller.cs
 *  PROJECT         : IISS
 *  PROGRAMMER      : Jody Markic, Nathan Bray & Gabriel Paquette
 *  FIRST VERSION   : 12/10/2016
 *  DESCRIPTION     :This file provides the Logger class that has method to write and remove logs to and
 *                   from the server event log.
 * 
 */

using System.Diagnostics;

namespace IISS
{
    //
    //  CLASS       : Logger
    //  DESCRIPTION : This class provides some functions to write and remove logs, it specifically logs,
    //                service events and exceptions that the Server throws.
    //
    public class Logger
    {
        private const string eventSourceName = "IISSLogger";
        private const string eventLogName = "IISSLogs";
        private static EventLog serviceEventLog;

        //
        //  METHOD      : Log()
        //  DESCRIPTION : This Method check to see if an eventLog source exists, if not it creates one,
        //                then logs the provided message.
        //  PARAMETER   : string : message
        //  RETURNS     : N/A
        //
        public static void Log(string message)
        {
            serviceEventLog = new EventLog();
            if (!EventLog.SourceExists(eventSourceName))
            {
                EventLog.CreateEventSource(eventSourceName, eventLogName);
            }
            serviceEventLog.Source = eventSourceName;
            serviceEventLog.Log = eventLogName;
            serviceEventLog.WriteEntry(message, EventLogEntryType.Information);
            
        }

        //
        //  METHOD      : removeLog()
        //  DESCRIPTION : This Method deletes a specified log from the Service event log.
        //  PARAMETER   : N/A
        //  RETURNS     : N/A
        //
        //public static void removeLog()
        //{
        //    serviceEventLog = new EventLog();
        //    if (EventLog.SourceExists(eventSourceName))
        //    {
        //        EventLog.DeleteEventSource(eventSourceName);
        //        EventLog.Delete(eventLogName);
        //    }
        //}
    }
}
