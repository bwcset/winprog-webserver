﻿/*
 *  FILENAME        : IISSInstaller.cs
 *  PROJECT         : IISS
 *  PROGRAMMER      : Jody Markic, Nathan Bray & Gabriel Paquette
 *  FIRST VERSION   : 12/10/2016
 *  DESCRIPTION     : This file holds the Main of the Service, it acts
 *                    as the main entry point for the service application.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IISS
{
    //
    //  CLASS       : Program
    //  DESCRIPTION : This class provides the Main() Method, the main entry point for the
    //                IIS Web Server solution
    //
    static class Program
    {
        //
        //  METHOD      : Main()
        //  DESCRIPTION : Main entry point for the application
        //  PARAMETER   : N/A
        //  RETURNS     : N/A
        //
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SETService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
