﻿/*
 *  FILENAME        : IISSInstaller.cs
 *  PROJECT         : IISS
 *  PROGRAMMER      : Jody Markic, Nathan Bray & Gabriel Paquette
 *  FIRST VERSION   : 12/10/2016
 *  DESCRIPTION     : This file holds the the IISS class that deals with install and uninstall events
 * 
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.IO;
using Configurations;
using System.Reflection;
using System.Diagnostics;

namespace IISS
{
    [RunInstaller(true)]
    public partial class IISSInstaller : Installer
    {
        public IISSInstaller()
        {
            InitializeComponent();
        }

        private void IISSServiceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            Logger.Log("IISS Web Server Installed Successfully.");            
        }

        private void IISSServiceInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            EventLog serviceEventLog = new EventLog();
            if (EventLog.SourceExists("IISSLogger"))
            {
                EventLog.DeleteEventSource("IISSLogger");
                EventLog.Delete("IISSLogs");
            }
        }
    }
}
