﻿/*
 *  FILENAME        : MainWindow.xaml.cs
 *  PROJECT         : IISS_Control_Panel
 *  PROGRAMMER      : Jody Markic, Gabriel Paquette, Nathan Bray
 *  FIRST VERSION   : 11/25/2016
 *  DESCRIPTION     : This file holds the code for the GUI of the IISS service.
 */

using System;
using System.Diagnostics;
using System.Windows;
using System.IO;
using Configurations;

namespace IISS_Control_Panel
{
    //
    //  CLASS       : MainWindows.xaml.cs
    //  DESCRIPTION : This is the class that handles all of the events for the IISS service GUI.
    //
    public partial class MainWindow : Window
    {
        EventLog iissLogger;
        private const string SERVICENAME = "IISS";
        private const int TIMEOUT = 10000;


        //
        //  METHOD      : MainWindow
        //  DESCRIPTION : This is the Contructor for the same window. It starts the event log.
        //
        public MainWindow()
        {
            InitializeComponent();
            if (EventLog.Exists("IISSLogs"))
            {
                iissLogger = new EventLog("IISSLogs");
                iissLogger.EntryWritten += new EntryWrittenEventHandler(MyOnEntryWritten);
                iissLogger.EnableRaisingEvents = true;
            }
        }


        //
        //  METHOD      : btnConfig_Click
        //  DESCRIPTION : This function loads the config file.
        //  RETURNS     : void
        //
        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(ConfigureServer.configFile))
            {
                Process.Start(ConfigureServer.configFile);
            }
            else
            {
                MessageBox.Show("Unable to locate configuration file at the following path: \n" + ConfigureServer.configFile);
            }
        }


        //
        //  METHOD      : Window_Loaded
        //  DESCRIPTION : Initalizes components when the window loads.
        //  RETURNS     : void
        //
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ConfigureServer.configureServer();
            try
            {
                toggleStartButton();
                lblIPAddr.Content += ConfigureServer._SERVER_IP;
            }
            catch (Exception ex)
            {
                lstEvents.Items.Add(DateTime.Now.ToString("hh:mm:ss tt") + " :\t " + ex.Message);
            }
        }


        //
        //  METHOD      : MyOnEntryWritten
        //  DESCRIPTION : Generates a time stamp when an entry is added to the log.
        //  RETURNS     : void
        //
        private void MyOnEntryWritten(Object source, EntryWrittenEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                lstEvents.Items.Add(e.Entry.TimeGenerated.ToString("hh:mm:ss tt") + " :\t " + e.Entry.Message);
            });
            toggleStartButton();
        }


        //
        //  METHOD      : btnStart_Click
        //  DESCRIPTION : Handles the event when the user clicks the start button to start the service.
        //  RETURNS     : void
        //
        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ServiceHandle.checkStatus(SERVICENAME) == "Running")
                {
                    ServiceHandle.StopService(SERVICENAME, TIMEOUT);
                }
                ServiceHandle.StartService(SERVICENAME, TIMEOUT);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        //
        //  METHOD      : toggleStartButton
        //  DESCRIPTION : Turns the service either on or off.
        //  RETURNS     : void
        //
        private void toggleStartButton()
        {
            Dispatcher.Invoke(() =>
            {
                if (ServiceHandle.checkStatus(SERVICENAME) == "Running")
                {
                    btnStart.Content = "Restart";
                    btnStop.IsEnabled = true;
                    Process[] test = Process.GetProcessesByName("IISS");
                    if (test[0] != null)
                    {
                        lblProcessID.Content = test[0].Id.ToString();
                        lblPortNum.Content = ConfigureServer._PORT;
                    }
                }
                else
                {
                    btnStart.Content = "Start";
                    btnStop.IsEnabled = false;
                    lblProcessID.Content = "";
                    lblPortNum.Content = "";
                }
            });
        }



        //
        //  METHOD      : btnStop_Click
        //  DESCRIPTION : Handles the event when the user clicks the stop button to stop the service.
        //  RETURNS     : void
        //
        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            if (ServiceHandle.checkStatus(SERVICENAME) == "Running")
            {
                ServiceHandle.StopService(SERVICENAME, TIMEOUT);
            }
        }
    }
}
