﻿/*
 *  FILENAME        : ServiceHandle.cs
 *  PROJECT         : IISS_Control_Panel
 *  PROGRAMMER      : Jody Markic, Gabriel Paquette, Nathan Bray
 *  FIRST VERSION   : 11/25/2016
 *  DESCRIPTION     : This file contains the logic to start and stop the service.
 *                    It also is able to check the status of the service.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IISS_Control_Panel
{
    //
    //  CLASS       : ServiceHandle
    //  DESCRIPTION : This class handles the start and stop of the service, as well as checks the status
    //                of the service during run time.
    //
    public class ServiceHandle
    {


        //
        //  METHOD      : StartService
        //  DESCRIPTION : This function starts the service, and specificies the time length of the timeout.
        //  PARAMETERS  : string : serviceName 
        //                int    : timeoutMilliseconds
        //  RETURNS     : void
        //
        public static void StartService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                service.Start();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
            catch (Exception e)
            {
                throw e;
            }
        }



        //
        //  METHOD      : StartService
        //  DESCRIPTION : This function stops the service, and specificies the time length of the timeout.
        //  PARAMETERS  : string : serviceName 
        //                int    : timeoutMilliseconds
        //  RETURNS     : void
        //
        public static void StopService(string serviceName, int timeoutMilliseconds)
        {
            ServiceController service = new ServiceController(serviceName);
            try
            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);
                
                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        //
        //  METHOD      : StartService
        //  DESCRIPTION : This function checks the current status of the service.
        //  PARAMETERS  : string : serviceName 
        //  RETURNS     : void
        //
        public static string checkStatus(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);

            switch (service.Status)
            {
                case ServiceControllerStatus.Running:
                    return "Running";
                case ServiceControllerStatus.Stopped:
                    return "Stopped";
                case ServiceControllerStatus.Paused:
                    return "Paused";
                case ServiceControllerStatus.StopPending:
                    return "Stopping";
                case ServiceControllerStatus.StartPending:
                    return "Starting";
                default:
                    return "Status Changing";
            }
        }



    }
}
