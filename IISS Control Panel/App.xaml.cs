﻿/*
 *  FILENAME        : App.xaml.cs
 *  PROJECT         : IISS_Control_Panel
 *  PROGRAMMER      : Jody Markic, Gabriel Paquette, Nathan Bray
 *  FIRST VERSION   : 11/25/2016
 */

using System.Windows;

namespace IISS_Control_Panel
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    }
}
